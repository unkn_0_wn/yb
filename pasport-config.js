const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy;
const UserAdmin = require('./models/UserA');
const bcrypt = require('bcryptjs');
const flash =require('express-flash');

  function initt(passport) {
    passport.use(new LocalStrategy({
        usernameField: 'email'
    }, async(email, passwrod, done) => {
        try{
            const user = await UserAdmin.findOne({email});
            if (!user){
                return done(null, false, {message: "No user with that email"})
            }
            if (await bcrypt.compare(passwrod, user.password)){
            return done(null,user);
            }else{
                return done (null, false, {message: "Passwrod incorrect "})
            }


        }catch (e) {
            console.log(e)
            return done(e)
        }
    }));


    passport.serializeUser(async function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(async function(id, done) {
       await UserAdmin.findById(id, function(err, user) {
            done(err, user);
        });
    });
}

module.exports = initt;