const puppeteer = require('puppeteer')

const brandCheck = async (requests, credentials, browser) => {
    var responses = []
    for (const request of requests) {
        responses.push(await checkTheBrand(request, credentials, browser))
    }
    console.log(responses)
    console.log('Done!')

    return responses
}

async function checkTheBrand(request, credentials, browser) {
    await console.log('qwe')
    const page = await browser.newPage()

    const wait = (ms) => new Promise(resolve => setTimeout(resolve, ms))

    await page.authenticate(credentials);

    try {
        await page.goto('http://base.uipv.org/searchBul/search.php?dbname=reestrtm', {waitUntil: 'networkidle0'})
        await wait(3000)

        //page.waitFor(20000)
    } catch (e) {
        throw e
    }

    await page.select('#searchbul > table > tbody > tr > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > table > tbody > tr:nth-child(1) > td > table > tbody > tr > td > select', '200')

    await page.type("#searchbul > table > tbody > tr > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > table > tbody > tr:nth-child(42) > td:nth-child(5) > input", request.brand)

    await page.type("#searchbul > table > tbody > tr > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > table > tbody > tr:nth-child(38) > td:nth-child(5) > input", request.numberkeyword)

    await page.click('#searchbul > table > tbody > tr > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > table > tbody > tr:nth-child(55) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > button', {waitUntil: 'networkidle0'})

    await wait(5000)
    //await page.waitFor(5000)

    let response

    for (let i = 3; ; i++) {
        const result = await page.evaluate((request) => {
            const message = document.querySelector('#searchbul > table > tbody > tr > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > table > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(1) > td > font')
            const searchResult = []
            if (message === null) {
                for (let i = 0; i < 620; i++) {
                    for (let j = 8; j < 10; j++) {
                        const resultElement = document.querySelector(`#searchbul > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr:nth-child(${j}) > td > table > tbody > tr:nth-child(${i}) > td.restitlecell > table > tbody > tr > td > a`)
                        if (resultElement === null || resultElement.innerText === null) {
                            continue
                        }
                        var brandVariations = resultElement.innerText.split('; ')
                        searchResult.push(brandVariations)
                        for (const brand of brandVariations) {
                            if (brand === request.brand.toLowerCase()) {
                                return `В классе ${request.namekeyword} марка ${request.brand} занята`
                            }
                        }
                    }
                }
                return `В классе ${request.namekeyword} марка ${request.brand} свободна`
            }
            switch (message.innerText) {
                case 'Не знайдено жодного запису':
                    return `В классе ${request.namekeyword} марка ${request.brand} свободна`
                case 'Помилка: Не задані умови пошуку' :
                    return 'Введены некорректные данные'
            }
        }, request)
        await wait(3000)
        //await page.waitFor(3000)
        try {
            await page.click(`#searchbul > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > a:nth-child(${i})`, {waitUntil: 'networkidle0'})
        } catch (err) {
            response = result
            break
        }
        await wait(2000)
        //await page.waitFor(2000)
    }
    return response
}

module.exports = brandCheck