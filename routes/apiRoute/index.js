const brandCheck = require('./parser');
const connectionSettings = require('./connectionSettings');
const express = require('express');
const puppeteer = require('puppeteer');
const shuffle = require('./shuffle')
var router = express.Router();

router.post(connectionSettings.endpoint, async function (req, res) {
    let response

    let browser

    addressArr = []

    for (const address of connectionSettings.proxies.addresses) {
        for (const protocol of connectionSettings.proxies.http.protocol) {
            for (const port of connectionSettings.proxies.http.ports) {
                addressArr.push({
                    address: address,
                    protocol: protocol,
                    port: port
                })
            }
            for (const protocol of connectionSettings.proxies.socks5.protocol) {
                for (const port of connectionSettings.proxies.socks5.ports) {
                    addressArr.push({
                        address: address,
                        protocol: protocol,
                        port: port
                    })
                }
            }
        }
    }

    shuffle(addressArr)



    for (let address of addressArr) {
        browser = await puppeteer.launch({
            headless: false,
            args: [`--proxy-server=${address.protocol}://${address.address}:${address.ports}`]
        })
        try {
            response = await brandCheck(req.body, connectionSettings.credentials, browser)
        } catch (e) {
            console.log(`${address.protocol}://${address.address}:${address.port} isn't accessible`)
            await browser.close()
            continue
        }
        res.send(response)
        break
    }

    await browser.close()
})

module.exports = router;
/*app.listen(connectionSettings.servicePort)

console.log(`Listening on port ${connectionSettings.servicePort}`)*/