const connectionSettings = {}

connectionSettings.servicePort = 5000

connectionSettings.endpoint = '/brandCheck'

connectionSettings.proxies = {
    addresses: [
        '176.114.8.153',
        '185.167.163.129',
        '91.207.60.30',
        '193.203.48.62',
        '194.38.21.30',
        '88.218.188.185',
        '93.179.69.66',
        '5.180.102.246',
        '139.28.37.203',
        '212.90.111.33'
    ],
    connections: {
        socks4: {
            protocol: ['socks4','socks5'],
            ports: [45786]
        },

        http: {
            protocol: ['http','https'],
            ports: [45785]
        }
    },
    http: {
        protocol: ['http', 'https'],
        ports: [65233]
    },

    socks5: {
        protocol: ['socks5'],
        ports: [65234]
    }
}

connectionSettings.credentials = {
    username: 'Selmaksvoloshynovskyi',
    password: 'D5c5HkX'
}

module.exports = connectionSettings
