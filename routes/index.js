var express = require('express');
var router = express.Router();
const Class = require('../models/class.js');
const Contact = require('../models/contact.js');
const Questions = require('../models/questions.js');
const Search = require('../models/search.js');
const About = require('../models/about.js');
const Brand = require('../models/brand.js');
const News = require('../models/news.js');
const nodemailer = require('nodemailer');
const http = require('http');
const path = require('path');
const fs = require('fs');
const os = require('os');
const url = require('url');

const transporter = nodemailer.createTransport({
    service: "Gmail",
    auth:{
        user:'winchester465@gmail.com',
        pass:'qwerty1234Q!'
    },
    tls: {
        rejectUnauthorized: false
    }
})
/* GET home page. */

router.get(['/', '/:l'],async (req, res, next) => {
    const langs = ["ua", "ru"];
    // req.langDefined = req.params.l || 'ru';

    if(req.params.l) {
        if (langs.indexOf(req.params.l) !== -1) {
            req.langDefined = req.params.l;

        }else{
            req.langDefined = 'ru';
        }
    } else {
        req.langDefined = 'ru';
    }

    const data = await JSON.parse(fs.readFileSync(path.normalize(path.join(__dirname, '..', '/', `${req.langDefined}.json`)), 'utf8'));
    req.translation = data;
    next();
},  async function(req, res, next) {
    let lanng =  req.langDefined || 'ru';
  let classModel = await Class.find();
  let contactModel = await Contact.find();
  let questionsModel = await Questions.find({language: `${lanng}`});
  let searchModel = await Search.find({language: `${lanng}`});
  let aboutModel = await About.find({language: `${lanng}`});
  let brandModel = await Brand.find();
  let newsModel = await News.find();
  let obj = {
    title: 'Express',
      classModel,
      questionsModel,
      searchModel,
      brandModel,
      newsModel,
      lanng,
      translation: req.translation
  }
  if(contactModel.length === 0){
       let newContact = new Contact({
           Phone: '044 123 45 62',
           Vadafone: '+38(099)999-99-99',
           Kievstar: '+38(096)999-99-99',
           Telegram: 'https://t.me/chanel',
           Viber: 'https://vb.me/',
           Messenger: 'https://m.me/',
           footerPhone: '044 765 43 21',
           Email: 'someEmail@gmail.com',
           Address: 'Atlantida',
       });
      newContact.save();
      obj.contactModel = newContact;
  }else{
      obj.contactModel = contactModel[0];
  }
  if(aboutModel.length === 0){
      let newAbout = new About({
        Title:'Привет',
          language: 'ru',

          Text:'Меня зовут Виталии я создал этот сайт для предпринимателей которые только начинают свой бизнес, и хотят его защитить,В 4% случаев торговую марку не позволяют зарегистрировать по не совсем конкретным причинам. В таком случаи я лично гарантирую компенсацию всей сумы.Пишите мне с предложениями или вопросами, я всегда рад ответить.',
           PersonalInstagram:'https://www.instagram.com/',
           PersonalFacebook:'https://www.facebook.com/',
           PersonalTelegram:'https://telegram.org/',
      });
      newAbout.save();
      let newAboutUA = new About({
          Title:'Привет',
          language: 'ua',
          Text:'Меня зовут Виталии я создал этот сайт для предпринимателей которые только начинают свой бизнес, и хотят его защитить,В 4% случаев торговую марку не позволяют зарегистрировать по не совсем конкретным причинам. В таком случаи я лично гарантирую компенсацию всей сумы.Пишите мне с предложениями или вопросами, я всегда рад ответить.',
          PersonalInstagram:'https://www.instagram.com/',
          PersonalFacebook:'https://www.facebook.com/',
          PersonalTelegram:'https://telegram.org/',
      });
      newAboutUA.save();
      obj.aboutModel = newAbout;
  }else{
      obj.aboutModel = aboutModel[0];
  }

  // console.log(req);
  // console.log(obj);
  res.render('index', obj);
});

router.post('/send_m', (req, res)=>{
    try {
        let htmlMail = `Імя:` + req.body.name + `<br /><br /> Тел: ` + req.body.number_phone + `<br /><br /> Назва бренда: ` + req.body.brand;
        if (req.body.selectItem) {
            let active_r = req.body.active.replace('✖', '');
            let time = (req.body.selectItem.step4 === 'short') ? '7-9' : '18-20',
                color = (req.body.selectItem.step3 === 800) ? 'С указанием цвета' : 'Без указания цвета',
                count_r = (req.body.selectItem.step5 === 'more') ? 'Несколько заявителей' : 'Один заявитель'
            htmlMail = `Імя:` + req.body.name +
                `<br /><br /> Тел: ` + req.body.number_phone +
                `<br /><br /> Назва бренда: ` + req.body.brand +
                `<br /><br /> Загальная стоимость : ${req.body.selectItem.total_price} грн.
              <br /><br /> Срок ${time} месяцов
              <br /><br /> ${color}
              <br /><br /> ${count_r}
              <br /><br /> Резидент Украины
              
`;
        }
        transporter.sendMail({
            from: 'YourBrand',
            to: 'hi@yourbrand.me',
            subject: 'Заявка на регистрацию ТМ',
            html: htmlMail
        }, (err, data) => {
            if (err) {
                console.error('Error', err);
                if (err.code === 'EDNS') {
                    return res.status(422).send(
                        {
                            code: err.code,
                            msg: 'your letter has not been sent'
                        })
                }
            } else {
                res.status(200).end();
            }
        })
    }catch (e) {
        console.error(e)
    }
})


module.exports = router;
