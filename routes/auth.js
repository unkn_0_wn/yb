var express = require('express');
var router = express.Router();
const UserAdmin = require('../models/UserA');
const passport = require('passport');
const falsh = require('express-flash');
const auth = require('../middleware/auth');
const bcrypt = require('bcryptjs');

router.get('/',  auth.mustAuthenticatedMw, async (req,res)=>{
    res.render('auth.ejs');
})
router.post('/', passport.authenticate('local', {
    successRedirect: '/adminpanel',
    failureRedirect: '/login',
    failureFlash: true
}))
router.post('/register',  async (req,res)=>{
    try {

        const {email, password} = req.body;
        const candidate = await UserAdmin.findOne({email})
        if(candidate){
            res.redirect('/login');
        }else{
            const hashPassword = await bcrypt.hash(password, 13)
            const user = new UserAdmin({
                email, password:hashPassword
            })
            user.save();
            res.status(200).redirect('/');
        }
    }catch (e) {
        console.log(e)
    }
})
module.exports = router;
