const  express = require('express');
const  router = express.Router();
const Class = require('../models/class.js');
const Contact = require('../models/contact.js');
const Questions = require('../models/questions.js');
const Search = require('../models/search.js');
const About = require('../models/about.js');
const Brand = require('../models/brand.js');
const News = require('../models/news.js');
const multer = require('../middleware/multer_config')
const auth = require('../middleware/auth');
//const http = require('http');
const os = require('os');

/* GET home page. */
router.get('/', auth.mustAuthenticatedMw ,async (req, res)=> {
   try{
    /*   console.log(os);
       console.log(http);*/
       // console.log(os.networkInterfaces());
       let  inf =os.networkInterfaces();
       let k =Object.keys(inf)[0];
       console.log(inf[k]);
       // console.log(os.hostname());
       let lang_br = req.headers["accept-language"].split(',');
       let lang_br2 = lang_br[1].split(';');
       if(lang_br2[0] !== 'ru' || lang_br2[0] !== 'ua'){
           lang_br2[0] = 'ru';
       }
       let classModel = await Class.find();
       let contactModel = await Contact.find();
       let questionsModel = await Questions.find({language: `${lang_br2[0]}`});
       let searchModel = await Search.find({language: `${lang_br2[0]}`});
       let aboutModel = await About.find({language: `${lang_br2[0]}`});
       let brandModel = await Brand.find();
       let newsModel = await News.find();
       let obj = {
           title: 'Admin',
           is: 'Admin',
           classModel,
           questionsModel,
           searchModel,
           brandModel,
           newsModel,
       }
       if(contactModel.length === 0){
            let newContact = new Contact({
                Phone: '044 123 45 62',
                Vadafone: '+38(099)999-99-99',
                Kievstar: '+38(096)999-99-99',
                Telegram: 'https://t.me/chanel',
                Viber: 'https://vb.me/',
                Messenger: 'https://m.me/',
                footerPhone: '044 765 43 21',
                Email: 'someEmail@gmail.com',
                Address: 'Atlantida',
            });
           newContact.save();
           obj.contactModel = newContact;
       }else{
           obj.contactModel = contactModel[0];
       }
       if(aboutModel.length === 0){
           let newAbout = new About({
                Title:'Привет',
                Text:'Меня зовут Виталии я создал этот сайт для предпринимателей которые только начинают свой бизнес, и хотят его защитить,В 4% случаев торговую марку не позволяют зарегистрировать по не совсем конкретным причинам. В таком случаи я лично гарантирую компенсацию всей сумы.Пишите мне с предложениями или вопросами, я всегда рад ответить.',
                language: 'ru',
                PersonalInstagram:'https://www.instagram.com/',
                PersonalFacebook:'https://www.facebook.com/',
                PersonalTelegram:'https://telegram.org/',
           });
           let newAboutua = new About({
               Title:'Привіт',
               Text:'Меня зовут Виталии я создал этот сайт для предпринимателей которые только начинают свой бизнес, и хотят его защитить,В 4% случаев торговую марку не позволяют зарегистрировать по не совсем конкретным причинам. В таком случаи я лично гарантирую компенсацию всей сумы.Пишите мне с предложениями или вопросами, я всегда рад ответить.',
               language: 'ua',
               PersonalInstagram:'https://www.instagram.com/',
               PersonalFacebook:'https://www.facebook.com/',
               PersonalTelegram:'https://telegram.org/',
           });
           newAbout.save();
           newAboutua.save();
           obj.aboutModel = newAbout;
       }else{
           obj.aboutModel = aboutModel[0];
       }
       res.render('admin.ejs', obj);
   }catch (e) {
       console.error(e)
   }
});

// save class name
router.post('/saveClass', async (req, res)=> {
     try{
         console.log(req.body);
         let l ='';
         if(req.body.nameClass){
             l ='ru';
         }else if (req.body.nameClassUA){
             l ='ua';
         }

         let findClass = await Class.find({ "NumberClass": req.body.selectNumClass});
         if(req.body && findClass.length === 0){
             let t = {};
             let newClass = new Class({
                 NumberClass: req.body.selectNumClass,
             });
             if(req.body.nameClass !== '' && req.body.nameClassUA !== ''){
                 let obj1 = {
                     text : req.body.nameClass,
                     language : 'ru'
                 }
                 let obj2 = {
                     text : req.body.nameClassUA,
                     language : 'ua'
                 }
                 newClass.NameClass = [obj1,obj2];

             } else if(req.body.nameClass !== ''){
                 t.text = req.body.nameClass;
                 t.language = l;
                 newClass.NameClass = t;
             }else if (req.body.nameClassUA !== ''){
                 t.text = req.body.nameClassUA;
                 t.language = l;
                 newClass.NameClass = t;
             }
             await newClass.save();
             return  res.json(newClass);
         }else if(req.body.nameClass !== '' && req.body.nameClassUA !== ''){
             console.log('ia')
             let findNameClass = findClass[0].NameClass;
             let t = {};
             let t2 = {};
             t.text = req.body.nameClass;
             t.language = 'ru';
             t2.text = req.body.nameClassUA;
             t2.language = 'ua';
             findNameClass.push(t);
             findNameClass.push(t2);
             Object.assign(findClass[0].NameClass, findNameClass);
             await findClass[0].save();
             return  res.status(203).json(findClass);
         }else if (req.body.nameClass !== ''){
             let findNameClass = findClass[0].NameClass;
             let t = {};
             t.text = req.body.nameClass;
             t.language = 'ru';
             findNameClass.push(t);
             Object.assign(findClass[0].NameClass, findNameClass);
             await findClass[0].save();
             return  res.status(202).json(findClass);
         }else if(req.body.nameClass === '' && req.body.nameClassUA !== ''){
             console.log(1)
             let findNameClass = findClass[0].NameClass;
             let t = {};
             t.text = req.body.nameClassUA;
             t.language = 'ua';
             findNameClass.push(t);
             Object.assign(findClass[0].NameClass, findNameClass);
             await findClass[0].save();
             return  res.status(202).json(findClass);
         }
     }catch (e) {
         console.error(e)
     }
});
//get class
router.post('/get', async (req, res)=> {
    try{
        let findClass = await Class.findById(req.body.id);
        return res.json(findClass);
    }catch (e) {
        console.error(e)
    }
});
//delete class
router.post('/dClassName', async (req, res)=> {
    try{
        let findClass = await Class.findByIdAndUpdate(req.body.id);
        findClass.NameClass = findClass.NameClass.filter(name=>{
            return name.text.toString().toLocaleLowerCase() !== req.body.text.toString().toLocaleLowerCase();
        })
        await findClass.save()
       res.json(findClass);
    }catch (e) {
        console.error(e)
    }
});
//update contact 
router.post('/updateEdges', async (req, res)=> {
    try{
        const {id} = req.body;
        delete req.body.id;
        let update = await Contact.findByIdAndUpdate(id, req.body);
        return res.json(update);
    }catch (e) {
        console.error(e)
    }
});
//new question or edit 
router.post('/newQ', async (req, res)=> {
    try{
        let {id} = req.body;
        delete req.body.id;
        if(id){
            const find = await Questions.findById(id);
            Object.assign(find, req.body);
            await find.save();
            return  res.status(202).json(find);
        }else{
            const newQuewstion = new Questions(req.body);
            await newQuewstion.save();
            return res.json(newQuewstion);
        }
     }catch (e) {
        console.error(e)
    }
});

//delete Question
router.post('/d', async (req, res)=> {
    try{
      let deleteQ = await Questions.findByIdAndDelete(req.body.id);
      return res.json(deleteQ);
    }catch (e) {
        console.error(e)
    }
});

// new Text or edit
router.post('/addSearch', async (req, res)=> {
    try{
      let {id} = req.body;
      delete req.body.id;
      if(id){
       let findS = await Search.findById(id);
       Object.assign(findS, req.body);
       await findS.save();

       return  res.status(202).json(findS);
      }else{
        let newSearch = new Search(req.body);
        await newSearch.save();
        return  res.json(newSearch);
      } 
    }catch (e) {
        console.error(e)
    }
});

//delete Search
router.post('/ds', async (req, res)=> {
    try{
      let deleteS = await Search.findByIdAndDelete(req.body.id);
      return res.json(deleteS);
    }catch (e) {
        console.error(e)
    }
});

//update About
router.post('/upMe', async (req, res)=> {
    try{
        const {id} = req.body;
        delete req.body.id;
        //let find = await About.findById(id);
        let update = await About.findByIdAndUpdate(id, req.body);
        return res.status(200).json(update);
    }catch (e) {
        console.error(e)
    }
});


//update or new brand
router.post('/sBrand', multer.single('img'), async (req, res)=> {
    try{

        let id = req.body.id_editOurBrand;
        delete req.body.id_editOurBrand;
        if(id){
            let find = await Brand.findById(id);
            let upBrand = {
                urlBrand: req.body.urlBrand,
                NameBrand: req.body.NameBrand
            };
            if(req.file){
                upBrand.img = req.file.path;
            }
            Object.assign(find, upBrand)
            await find.save();
            return res.status(202).json(find);
        }else{
            let newBrand = new Brand({});
            Object.assign(newBrand, req.body);
            if(req.file){
                newBrand.img = req.file.path;
            }
            newBrand.save();
            return res.status(200).json(newBrand);
        }

    }catch (e) {
        console.error(e)
    }
});

//delete brand
router.post('/dBrand', async (req, res)=> {
    try{
        let deleteBrand = await Brand.findByIdAndDelete(req.body.id);
        return res.json(deleteBrand);
    }catch (e) {
        console.error(e)
    }
});
//update or new news
router.post('/news', async (req, res)=>{
    try {

        let {id} = req.body;
        delete req.body.id;
        if(id){
            let findN = await News.findById(id);
            Object.assign(findN, req.body);
            await findN.save()
            return res.status(202).json(findN)
        }else {
           let newNews = new News(req.body);
            newNews.save();
           res.status(200).json(newNews);
        }
    }catch (e) {
        console.error(e)
    }
})

//delete news
router.post('/dNews', async (req, res)=> {
    try{
        let deleteNews = await News.findByIdAndDelete(req.body.id);
        return res.json(deleteNews);
    }catch (e) {
        console.error(e)
    }
});
//logout
router.post('/logout', async (req,res)=>{
    try{
        req.session.destroy(()=>{
            res.redirect('/');
        });
    }catch (e) {
        console.error(e)
    }
})

//get language question
router.post('/getQuestions', async (req,res)=>{
    try {
        let question = await Questions.find({language: `${req.body.lang}`});
        return res.json(question);
    }catch (e) {
        console.error(e)
    }
})

router.post('/getTextLang', async (req,res)=>{
    try {
        let sear = await Search.find({language: `${req.body.lang}`});
        return res.json(sear);
    }catch (e) {
        console.error(e)
    }
})

router.post('/getAbout', async (req,res)=>{
    try {

        let Abbout = await About.find({language: `${req.body.lang}`});
        return res.json(Abbout);
    }catch (e) {
        console.error(e)
    }
})


router.post('/rebase', async (req,res)=>{
    try {
        let classModel = await Class.find();

        /*let reclasses =*/
       let r = classModel.map(async (item)=>{
           item.NameClass.forEach((nClass, index)=>{
               if (nClass._id === undefined && nClass.text === undefined && nClass.language === undefined) {
                   let obj = {
                       language: 'ru'
                   }
                   let textO = '';
                   let j = JSON.stringify(nClass);
                   let j2 = JSON.parse(j);
                   for (let j2Key in j2) {
                       textO += j2[j2Key]
                   }
                   obj.text = textO;
                   item.NameClass[index] = obj;
               }
            })
           await Class.findByIdAndUpdate(item._id, item);
           return item;
        })
        return res.status(200).end();
    }catch (e) {
        console.error(e)
    }
})


module.exports = router;
