var express = require('express');
var multer  = require('multer');
var path = require('path');


// Set Storage Engine
const storage = multer.diskStorage({
    destination(req, file, cb){
        cb(null, './img')
    },
    filename: (req,file,cb)=> {
      cb(null, file.fieldname + '_' + new Date().toISOString().replace(/:/g,'-') + path.extname(file.originalname));
    }
  });

const category_storage = multer.diskStorage({})

//Filter File
const fileFilter = (req, file, cb) => {
    if(file.mimetype === "image/png" ||
    file.mimetype === "image/jpg"||
    file.mimetype === "image/jpeg"){
        cb(null, true);
    }
    else{
      // console.log(file.originalname);
      req.fileValidationError = file;
        cb(null, false,new Error('goes wrong on the mimetype'));
    }
  }
//Init Multer
const upload = multer({
    storage: storage,
    fileFilter: fileFilter,
    limits: { fileSize: 1 * 4096 * 4096 },
  });



module.exports.fileFilter = fileFilter;
module.exports = upload;
