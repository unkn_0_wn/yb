exports.mustAuthenticatedMw = function (req, res, next){
    if(!req.isAuthenticated() && req.baseUrl !== '/login'){
        return res.redirect('/')
    }
    if( req.isAuthenticated() && req.baseUrl === '/login'){
        return res.redirect('/adminpanel');
    }
    next()
};