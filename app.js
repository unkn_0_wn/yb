var createError = require('http-errors');
var express = require('express');
const session = require('express-session');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require("body-parser");
const mongoose = require('mongoose');
const passport = require('passport');
var i18n = require("i18n-express");
const { I18n } = require('i18n');
const inittPassprot= require('./pasport-config')
const MongoStore = require('connect-mongodb-session')(session);
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var adminRouter = require('./routes/admin.js');
var authRouter = require('./routes/auth.js');

const brandCheck = require('./routes/apiRoute/parser');
const connectionSettings = require('./routes/apiRoute/connectionSettings');
const puppeteer = require('puppeteer');
const shuffle = require('./routes/apiRoute/shuffle.js');

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}))
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
const store = new MongoStore({
  collection: 'sessions',
  uri: ' mongodb://127.0.0.1:27017/yb',

})
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/img',express.static(__dirname + '/img'));

app.use(session({
  secret: 'keyssSESSIONSECRET',
  resave: true,
  saveUninitialized: true,
  store
}))
/*app.use( i18n({
  translationsPath: path.normalize(path.join(__dirname, '/')), // <--- use here. Specify translations files path.
  siteLangs: ["ua","ru"],
  textsVarName: 'translation',
  defaultLang: 'ru',
}) );*/
app.use(passport.initialize());
app.use(passport.session());
/*console.log('================================');

console.log('================================');*/


inittPassprot(passport);

app.use('/',indexRouter);

app.use('/users', usersRouter);
app.use('/adminpanel', adminRouter);
app.use('/login', authRouter);
//app.use('/brandCheck', parRouter);
//app.use(fileMiddlewere.single('imgg'));

/*
app.post('/brandCheck', async function (req, res) {
  let response
  console.log(req.body);
  let browser

  addressArr = []

  for (const address of connectionSettings.proxies.addresses) {
    for (const connection in connectionSettings.proxies.connections) {
      if (connectionSettings.proxies.connections[connection].ports.length !== 0) {
        for (const protocol of connectionSettings.proxies.connections[connection].protocol) {
          for (const port of connectionSettings.proxies.connections[connection].ports) {
            addressArr.push({
              address: address,
              protocol: protocol,
              port: port
            })
          }
        }
      }
    }

  }

  shuffle(addressArr)


  for (let address of addressArr) {
    browser = await puppeteer.launch({
      headless: false,
      args: [`--proxy-server=${address.protocol}://${address.address}:${address.port}`]
    })
    try {
      response = await brandCheck(req.body, connectionSettings.credentials, browser)
    } catch (e) {
      console.log(e)
      await browser.close()
      continue
    }
    res.send(response)
    break
  }
  await browser.close();
})
*/
app.post('/brandCheck', async function (req, res) {
  await console.log('/brandCheck', connectionSettings.proxies.addresses)
  let response
  console.log(req.session)
  let browser
  delete req.session.countD;
  delete req.session.toDate;
  req.session.save();
  if(req.session.countD){
    req.session.countD++;
    req.session.save()

  }else{
    req.session.countD = 1
    req.session.toDate =Date.now() + 24 * 60 * 60 * 1000;
    req.session.save();
  }
  if (req.session.countD === 3 && Date.now() < req.session.toDate){
    response = ["limit"]
    req.session.countD--;
    console.log('now', Date.now());
    console.log('date', req.session.toDate);
    return  res.send(response)
  }else if (Date.now() >= req.session.toDate){
    req.session.countD = 1;
    req.session.toDate = Date.now() + 24 * 60 * 60 * 1000;
    req.session.save()
  }
  console.log(req.session);
  let addressArr = []

  for (let address of connectionSettings.proxies.addresses) {
    console.log('/brandCheck address', address)
    for (let connection in connectionSettings.proxies.connections) {
      console.log('/brandCheck connection', connection)
      for (let protocol of connectionSettings.proxies.connections[connection].protocol) {
        for (let port of connectionSettings.proxies.connections[connection].ports) {
          console.log('/brandCheck port', port)
          addressArr.push({
            address: address,
            protocol: protocol,
            port: port
          })
        }
      }

    }

  }

  shuffle(addressArr)


  for (let address of addressArr) {
    browser = await puppeteer.launch({
      headless: true,
      args: ['--no-sandbox', '--disable-setuid-sandbox',`--proxy-server=${address.protocol}://${address.address}:${address.port}`]
    })
    try {
      response = await brandCheck(req.body, connectionSettings.credentials, browser)
    } catch (e) {
      console.log(e)
      //await browser.close()
      continue
    }
    res.send(response)
    break
  }
  await browser.close()
})
// mongoose
async function ms () {
  await  mongoose.connect( ' mongodb://127.0.0.1:27017/yb', {useNewUrlParser:true,
    useUnifiedTopology: true,
    useFindAndModify: false})

}ms();

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
