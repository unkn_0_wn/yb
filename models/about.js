const {Schema, model} = require('mongoose');
const About = new Schema({
    Title:{
        type:String,
        required:true
    },
    Text: {
        type:String,
        required:true
    },
    language:{
        type:String,
        required:true
    },
    PersonalInstagram:{
        type:String,
        required:true
    },
    PersonalFacebook:{
        type:String,
        required:true
    },
    PersonalTelegram:{
        type:String,
        required:true
    },
});



module.exports = model('about', About);