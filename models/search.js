const {Schema, model} = require('mongoose');
const Search = new Schema({
    SearchText:{
        type:String,
        required:true
    },
    language:{
        type:String,
        required:true
    }

});



module.exports = model('search', Search);