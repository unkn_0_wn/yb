const {Schema, model} = require('mongoose');
const Class = new Schema({
    NumberClass:{
        type:Number,
        required:true
    },

    NameClass:[
        {
            text:{
                type:String,
                required:true
            },
            language:{
                type:String,
            }
        }

    ],
});



module.exports = model('class', Class);