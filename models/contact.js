const {Schema, model} = require('mongoose');
const Contact = new Schema({
    Phone:{
        type:String,
        required:true
    },
    Vadafone:{
        type:String,
        required:true
    },
    Kievstar:{
        type:String,
        required:true
    },
    Telegram:{
        type:String,
        required:true
    },
    Viber:{
        type:String,
        required:true
    },
    Messenger:{
        type:String,
        required:true
    },
    footerPhone:{
        type:String,
        required:true
    },
    Email:{
        type:String,
        required:true
    },
    Address:{
        type:String,
        required:true
    }

});



module.exports = model('contact', Contact);