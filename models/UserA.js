const {Schema, model} = require('mongoose');
const UserAdminSchema = new Schema({
    email:{
        type: String,
        required:true
    },
    password:{
        type:String,
        required: true
    }
})

module.exports = model('UserAdmin', UserAdminSchema);