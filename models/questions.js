const {Schema, model} = require('mongoose');
const Questions = new Schema({
    Question:{
        type:String,
        required:true
    },
    Answer:{
            type:String,
            required:true
        },
    language:{
        type:String,
        required:true
    }

});



module.exports = model('question', Questions);