const {Schema, model} = require('mongoose');
const News = new Schema({
    newsTitle:{
        type:String,
        required:true
    },
    newsText:
        {
            type:String,
            required:true
        },
    newsDate:{
        type:String,
        required:true
    }
});



module.exports = model('news', News);