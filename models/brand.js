const {Schema, model} = require('mongoose');
const Brand = new Schema({
    img:{
        type:String,
        required:true
    },
    urlBrand:
        {
            type:String,
            required:true
        },
    NameBrand:{
        type:String,
        required:true
    }
});



module.exports = model('brand', Brand);