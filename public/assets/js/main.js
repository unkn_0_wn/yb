$(document).ready(function () {

      var keyBrand,
          nameBrand;
      //Set option selected onchange
     $('#tokenfield').change(function(){
       var value = $(this).val();
       $("#kBr").val(value);
       $("#nameBr").val($("#select2-tokenfield-container").text());
     });
     $('#id_brand').change(function(){
       var value = $(this).val();
       $("#nameC").val(value);
     });

    //disable enter keypress at form
    $('.disabled-enter').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    function dotsAnimation(){
        let dots = $('.loading-dots span');
        let a = 0;
    //     if ($('.loading-dots span.active').index() === dots.length - 1 || a ===0 ){
    //         $('.loading-dots span').eq(0).addClass('active').siblings().removeClass('active');
    //         a++;
    //     }
    //     else {
    //         $('.loading-dots span.active').next().toggleClass('active').siblings().removeClass('active');
    //     }
    //
    let timerId = setTimeout(function tick () {

            if ($('.loading-dots span.active').index() === dots.length - 1 || a ===0 ){
                $('.loading-dots span').eq(0).addClass('active').siblings().removeClass('active');
                a++;
            }
            else {
                $('.loading-dots span.active').next().toggleClass('active').siblings().removeClass('active');
            }
            timerId = setTimeout(tick, 600);
        },
          600);

        // setTimeout(() => {  if ($('.loading-dots span.active').index() === dots.length - 1 || a ===0 ){
        //     $('.loading-dots span').eq(0).addClass('active').siblings().removeClass('active');
        //     a++;
        // }
        // else {
        //     $('.loading-dots span.active').next().toggleClass('active').siblings().removeClass('active');
        // } }, 400);
    }
    dotsAnimation();
    function brandCheck() {
        validate = false;
        var data = '';
        var request = new XMLHttpRequest();
        var url = "/brandCheck";

        request.open("POST", url, true);
        request.setRequestHeader("Content-Type", "application/json");
        request.upload.onprogress = function (event) {
            $('.registration__card').removeClass('active').fadeOut('slow');
            $('.registration__card_type_loading').addClass('active').fadeIn('slow');
            dotsAnimation();
            //console.log(event);
        };
        //request.timeout = 2500;
        request.onreadystatechange = function () {
            console.log('status',request.status );
            if (request.readyState === 4 && request.status === 200) {
                var jsonData = JSON.parse(request.response);
                /*obj.form.reset();*/
                var s =  jsonData.join();
                var m=s.split(' ');
                //console.log(m[m.length-1]);
                if (m[m.length-1] == "свободна") {

                    let classC = '.registration__card_type_true';
                    $('.registration__card').removeClass('active');
                    $('.alert__brand.true .mark').text(jsonData[0] + `, вы можете заполнить форму для консультации или проверте другое название`);
                    $('#registration__card').fadeOut('slow').removeClass('active');
                    $(classC).fadeIn('slow').addClass('active');
                    $('.mark').text($('#nameC').val());
                    $('.n').text('"' + $('#nameBr').val() + '"');
                } else if (m[m.length-1] == "занята") {
                    let classCs = '.registration__card_type_false';
                    $('.registration__card').removeClass('active');
                    $('#registration__card').fadeOut('slow').removeClass('active');
                    $('.alert__brand.false .mark').text(jsonData[0] + `, вы можете заполнить форму для консультации или проверте другое название`);
                    $(classCs).fadeIn('slow').addClass('active');
                    $('.mark').text($('#nameC').val());
                    $('.n').text($('#nameBr').val());
                }else if(m[m.length-1] == "limit"){
                    let classCs = '.registration__card_type_limit';
                    $('.registration__card').removeClass('active');
                    $('#registration__card').fadeOut('slow').removeClass('active');
                    $(classCs).fadeIn('slow').addClass('active');
                    $('.mark').text($('#nameC').val());
                    $('.n').text($('#nameBr').val());
                }
            }
            /*else if(request.status === 0 ){
                console.log('zero')
                var jsonData = JSON.parse(request.response);
                console.log(jsonData);

            }*/
        };

        var k =  document.getElementById("kBr").value; //
        var nB = document.getElementById("nameBr").value;
        var nameC = document.getElementById("nameC").value;
        data = JSON.stringify([{
            "brand": nameC,
            "numberkeyword": k,
            "namekeyword": nB,
        }]);
        console.log(data);
        request.send(data);

    }
    console.log();
    document.getElementById('first_check').addEventListener('click',function (e) {
        e.preventDefault();

        if(document.getElementById('id_brand').value.length === 0 || document.querySelector('.selectitem') === null ){
            return 0;
        }
        $('.false_brand_name').text($('#id_brand').val());
        $('.false_active_brand').text($('.selectitem').text().replace('✖', ''));
        $('.true_name_brand').text($('#id_brand').val());
        $('.true_active_brand').text($('.selectitem').text().replace('✖', ''));

        $('.header__social-text').hide();
        let currentCard =  this.closest('.registration__card');
        let nextCard = currentCard.nextSibling.nextSibling;
        brandCheck();
    });

    //faq

    // let faqButts = document.querySelectorAll('.faq__wrap ul li > a');
    // for (let i = 0 ; i < faqButts.length ; i++){
    //     faqButts[i].addEventListener('click',function (e) {
    //         e.preventDefault();
    //         $(this).parents('li').parent().find('.active').find('.faq__hidden').slideToggle();
    //         $(this).parents('li').find('.faq__hidden').slideToggle('slow').parent().toggleClass('active').siblings().removeClass('active');
    //     },false);
    // }
    $('.faq__wrap ul li a').click( function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $(this).next().slideToggle();
    } );
    // tokenfield focus
    let tokenfield = document.getElementById('tokenfield');
    tokenfield.onfocus = function (){
        $('.tags-list').addClass('active');
    };

    tokenfield.onblur = function (){
        $('.tags-list').removeClass('active');
    };
    // reveal anmimation
    ScrollReveal().reveal('.reveal');

    $("#check_other_brand").click( function () {
        $('.registration__card').fadeOut('slow').removeClass('active');
        $('#registration__card_first').fadeIn('slow').addClass('active');
    } );
});
