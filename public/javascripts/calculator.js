(function (){
    const div = document.querySelector('div.calc_custom_button');
    const l_div = document.querySelector('div.step_calc-fi-cost');
    const h2 = l_div.querySelector('h2.total_cost');
    const span = l_div.querySelector('span.stp3_value');
    const ul = l_div.querySelector('.yourchoise_pre_step');
    const plus = document.querySelector('span.plus_btn');
    const minus = document.querySelector('span.minus_btn');
    const selectItem = {}, data = {};
    const priceList = {
        step1 : 3200,
        step2: 4000,
        step21: 5220,
        step22:9396,
        step211: 612,
        step221:960,
        step3: 800,
    };
    let total_price = 0;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        document.querySelector('.tab_triger__item-active').classList.remove('tab_triger__item-active');
        document.querySelector('.tab_content-active').classList.remove('tab_content-active');

            document.querySelector('.tabs_trigers').addEventListener('click', (e)=>{
                document.querySelector('.close_btn_m_t').classList.add('active');
                document.body.classList.add('body_ov_h');
            })
            document.querySelector('.close_btn_m_t').addEventListener('click', e=>{
                document.querySelector('.close_btn_m_t').classList.remove('active');
                document.body.classList.remove('body_ov_h');
                document.querySelector('.tab_triger__item-active').classList.remove('tab_triger__item-active');
                document.querySelector('.tab_content-active').classList.remove('tab_content-active');
            })
        document.querySelectorAll('h4.registration__side_bar-title').forEach(item=> {

            item.addEventListener('click', e => {
                e.target.nextElementSibling.classList.toggle('active');
            })
        })



    }

    function totalP (){
        h2.innerText = (isNaN(selectItem.total_price_round )) ? 0 + ' ' + ' грн' : selectItem.total_price_round + ' ' + ' грн';
       if(selectItem.step4 === 'short'){
           span.innerText = '7-9';
           ul.querySelector('.duration_time').innerText = 'Срок 7 - 8 месяцов';
       }else{
           span.innerText = '18 - 20';
           ul.querySelector('.duration_time').innerText = 'Срок 18 - 20 месяцов';
       }
        if (selectItem.step3 === 800){
            ul.querySelector('.color_log').innerText = 'C указанием цвета';
        }else {
            ul.querySelector('.color_log').innerText = 'Без указания цвета';
        }
        if (selectItem.step2 === 'verbal'){
            ul.querySelector('.style_log').innerText = 'Словесный тип';
        }else if(selectItem.step2 === 'graphic'){
            ul.querySelector('.style_log').innerText = 'Графический тип';

        } else if(selectItem.step2 === 'combined') {
            ul.querySelector('.style_log').innerText = 'Комбинированый тип';
        }
        if(selectItem.step5 === 'one'){
            ul.querySelector('.style_log').innerText = 'Один заявитель';
        }else if(selectItem.step5 === 'more'){
            ul.querySelector('.style_log').innerText = 'Несколько заявителей';
        }

    }



    div.addEventListener('click', e=>{
        e.preventDefault();
        if (e.target.closest('.plus_btn')){
            document.getElementById('count_mar').value=  +document.getElementById('count_mar').value + 1;
        }else if(e.target.closest('.minus_btn')){
            if(document.getElementById('count_mar').value > 0){
                document.getElementById('count_mar').value=  +document.getElementById('count_mar').value - 1;
            }
        }
    })
    document.getElementById('next_step_calc_f').addEventListener('click', e=>{
        e.preventDefault();
        if (e.target.closest('.active')){
            document.querySelector('.step_calc-first').classList.remove('active');
            document.querySelector('.step_calc-first').nextElementSibling.classList.add('active');
            selectItem.count = +document.getElementById('count_mar').value;
        }
    })


    document.querySelector('.step2_radio_button').addEventListener('click', e=>{
        document.querySelectorAll('input[name="contact"]').forEach(item=>{
            if (item.checked){
                selectItem.step2 = item.value;
            }
        })
        if(selectItem.step2){
            document.querySelector('.step_calc-second').classList.remove('active');
            document.querySelector('.step_calc-second').nextElementSibling.classList.add('active');
        }
    })
    document.querySelector('.step3_radio_but').addEventListener('click', e=>{
        document.querySelectorAll('input[name="colorC"]').forEach(item=>{
            if (item.checked){
                selectItem.step3 = +item.value;
            }
        })
        if(selectItem.step3 || selectItem.step3 === 0){
            document.querySelector('.step_calc-third').classList.remove('active');
            document.querySelector('.step_calc-third').nextElementSibling.classList.add('active');
        }
    })
    document.querySelector('.step4_radio_but').addEventListener('click', e=>{
        document.querySelectorAll('input[name="countR"]').forEach(item=>{
            if (item.checked){
                selectItem.step4 = item.value;
            }
        })
        if(selectItem.step4){
            document.querySelector('.step_calc-fourth').classList.remove('active');
            document.querySelector('.step_calc-fourth').nextElementSibling.classList.add('active');
        }
    })
    document.querySelector('.step5_radio_but').addEventListener('click', e=>{
        document.querySelectorAll('input[name="applicant"]').forEach(item=>{
            if (item.checked){
                selectItem.step5 = item.value;
            }
        })
        if(selectItem.step5){
            selectItem.total_price = 0;
            total_price = 0;
            total_price += priceList.step1 * selectItem.count;
            if(selectItem.step4 === 'long'){
                total_price += 4000;
            }else if (selectItem.step4 === 'short' && selectItem.step2 === 'verbal' || selectItem.step2 === 'graphic'){
                total_price += 5000 + 5220;
                total_price += selectItem.count-1 * 612;
            }else if(selectItem.step4 === 'short' && selectItem.step2 === 'combined'){
                total_price += 5000 + 9396;
                total_price += ((selectItem.count - 1) * 960);
            }
            total_price += selectItem.step3;
            if(selectItem.step5 === 'more'){
                total_price = total_price + (total_price / 10) * 3;
            }
            selectItem.total_price = total_price;
            selectItem.total_price_round = Math.round(total_price);
            document.querySelector('.step_calc-fifth').classList.remove('active');
            totalP();
            document.querySelector('.step_calc-fifth').nextElementSibling.classList.add('active');
        }
    })

    // document.querySelector('#first_check2').addEventListener('click', e=>{
    //     e.preventDefault();
    //     if(document.getElementById('id_brand2').value.length > 0 && document.querySelector('span.selectitem2')) {
    //         data.brand = document.getElementById('id_brand2').value;
    //         data.active = document.querySelector('span.selectitem2').innerText;
    //         data.key_br = document.getElementById('tokenfield2').options[0].value;
    //
    //         document.querySelector('.first_send_m').classList.remove('active');
    //         document.querySelector('.first_send_m').nextElementSibling.classList.add('active');
    //     }
    // })

    document.querySelectorAll('.send_m_request').forEach(item=> {
        // if(document.getElementById('id_brand2').value.length === 0 || document.getElementById('s_name2').value.length === 0 || document.getElementById('tel_s2').value.length === 0){
        //     return 0;
        // }
        item.addEventListener('click', async e => {
            e.preventDefault();
            try {
                if( e.target.classList.contains('first_check3_cls') && (document.getElementById('id_brand2').value.length === 0) || document.getElementById('s_name2').value.length === 0 || document.getElementById('tel_s2').value.length === 0){
                    console.log(document.getElementById('id_brand2').value.length);
                    console.log(document.getElementById('s_name2').value.length);
                    console.log(document.getElementById('tel_s2').value.length);
                    return 0;
                }
                e.target.closest('button.send_m_request').lastElementChild.classList.add('active');
                e.target.closest('button.send_m_request').firstElementChild.classList.add('dis_n');
                if(document.querySelector('.registration__card_type_true').classList.contains('active')){
                    data.brand = document.getElementById('id_brand').value;
                    data.active = document.querySelector('span.selectitem').innerText;
                    data.key_br = document.getElementById('tokenfield').options[0].value;
                    data.name = document.getElementById('s_name').value;
                    data.number_phone = document.getElementById('tel_s').value;
                }else if(document.querySelector('.registration__card_type_false').classList.contains('active')){
                    data.brand = document.getElementById('id_brand').value;
                    data.active = document.querySelector('span.selectitem').innerText;
                    data.key_br = document.getElementById('tokenfield').options[0].value;
                    data.name = document.getElementById('s_name_false').value;
                    data.number_phone = document.getElementById('s_tel_true').value;
                }else{
                    data.brand = document.getElementById('id_brand2').value;
                    data.name = document.getElementById('s_name2').value;
                    data.number_phone = document.getElementById('tel_s2').value;
                }

                if(selectItem.step5){
                    data.selectItem = selectItem;
                }

                const res = await fetch('/send_m', {
                    method: 'POST', // или 'PUT'
                    body: JSON.stringify(data), // данные могут быть 'строкой' или {объектом}!
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                document.getElementById('id_brand2').value = '';
                document.getElementById('s_name2').value = '';
                document.getElementById('tel_s2').value = '+380'
                if (res.status === 200) {
                    e.target.closest('button.send_m_request').lastElementChild.classList.remove('active');
                    e.target.closest('button.send_m_request').firstElementChild.classList.remove('dis_n');
                    if(document.querySelector('.first_send_m').classList.contains('active')) {
                        document.querySelector('.first_send_m').classList.remove('active');
                        document.querySelector('.first_send_m').parentElement.lastElementChild.classList.add('active');
                    }else if (document.querySelector('.registration__card_type_true').classList.contains('active')){
                        document.querySelector('.registration__card_type_true').classList.remove('active');
                        document.querySelector('.registration__card_type_true').parentElement.lastElementChild.classList.add('active');
                    }else if (document.querySelector('.registration__card_type_false').classList.contains('active')){
                        document.querySelector('.registration__card_type_false').classList.remove('active');
                        document.querySelector('.registration__card_type_false').parentElement.lastElementChild.classList.add('active');
                    }
                }else{
                    e.target.closest('button.send_m_request').lastElementChild.classList.remove('active');
                    e.target.closest('button.send_m_request').firstElementChild.classList.remove('dis_n');
                }
            } catch (e) {
                console.error(e);
            }

        })
    })
}())