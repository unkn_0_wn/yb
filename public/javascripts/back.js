// switch lang
document.querySelector('button.reset_base').addEventListener('click', async (e)=>{
    try {
        let data = {};
        const response = await fetch('/adminpanel/rebase', {
            method: 'POST', // или 'PUT'
            body: JSON.stringify(data), // данные могут быть 'строкой' или {объектом}!
            headers: {
                'Content-Type': 'application/json'
            }
        })
        console.log(response);
    }catch (e) {
        console.error(e)
    }
})


document.querySelectorAll('.lang_item').forEach(item=>{
    item.addEventListener('click', async e=>{
       try{
           e.target.parentElement.querySelector('.active_lang').classList.remove('active_lang');
           e.target.classList.add('active_lang');
           if (e.target.parentElement.classList.contains('select_lang_question')){
               let data = {
                   lang : e.target.dataset.lang
               }
               const response = await fetch('/adminpanel/getQuestions', {
                   method: 'POST', // или 'PUT'
                   body: JSON.stringify(data), // данные могут быть 'строкой' или {объектом}!
                   headers: {
                       'Content-Type': 'application/json'
                   }
               });
               if(response.status === 200 && response.ok){
                   let json = await response.json();
                   document.querySelectorAll('.quest__li').forEach(item=>{
                       item.remove();
                   })
                   json.forEach(item=>{
                       let htmlQ = `<li class="collection-item quest__li "><div>${item.Question}<a href="" data-language="${item.language}" data-answer="${item.Answer}" data-id="${item._id}" class="secondary-content id__e_element"><i class="material-icons editEl">create</i>&nbsp;<i class="material-icons editDel">clear</i></a></div></li>`;
                       document.querySelector('ul#list__question').insertAdjacentHTML('beforeend', htmlQ);
                       document.querySelector('input#quest-title').value = '';
                       document.querySelector('textarea#textarea1').value = '';
                   })
               }
           }else if (e.target.parentElement.classList.contains('select_lang_text')){
               let data = {
                   lang : e.target.dataset.lang
               }
               const response = await fetch('/adminpanel/getTextLang', {
                   method: 'POST', // или 'PUT'
                   body: JSON.stringify(data), // данные могут быть 'строкой' или {объектом}!
                   headers: {
                       'Content-Type': 'application/json'
                   }
               });
               if(response.status === 200 && response.ok){
                   let json = await response.json();
                   document.querySelectorAll('.text_item_search').forEach(item=>{
                       item.remove();
                   })
                   json.forEach(item=>{
                       let html = `<li class="collection-item text_item_search"><div>${item.SearchText}<a href="#" data-language="${item.language}" data-id="${item._id}" class="secondary-content search__id"><i class="material-icons search_edit">create</i>&nbsp;<i class="material-icons search_delete">clear</i></a></div></li>`;
                       document.querySelector('ul.text__search_ul').insertAdjacentHTML('beforeend', html);
                       document.querySelector('input#quest-s').value = '';
                   })
               }
           }else if (e.target.parentElement.classList.contains('select_lang_about')){
               let data = {
                   lang : e.target.dataset.lang
               }
               const response = await fetch('/adminpanel/getAbout', {
                   method: 'POST', // или 'PUT'
                   body: JSON.stringify(data), // данные могут быть 'строкой' или {объектом}!
                   headers: {
                       'Content-Type': 'application/json'
                   }
               });
               if(response.status === 200 && response.ok){
                   let json = await response.json();
                   document.querySelector('textarea#textarea2About').value = json[0].Text;
                   document.getElementById('hid_ab_id').value = json[0]._id;
                   document.getElementById('about-title').value = json[0].Title;
               }

           }
       }catch (e) {
           console.error(e)
       }

    })
})


//Save new Class or update
document.querySelector('button.save-cla[name="action"]').addEventListener('click', async ()=>{
    try{
        let data = {}
        if(+document.querySelector('select#selectNumClass').value === 0 ){
            alert('Не выбран номер класа');
        }else if(document.querySelector('input#name_class[name="name_class"]').value.toString() === "" && document.querySelector('input#name_class_ua[name="name_class_ua"]').value.toString() === ""){
            alert('Введите название класа');
        }else{
            data.selectNumClass = +document.querySelector('select#selectNumClass').selectedIndex;
            data.nameClass = document.querySelector('input#name_class[name="name_class"]').value.toString();
            data.nameClassUA = document.querySelector('input#name_class_ua[name="name_class_ua"]').value.toString();
            if(document.querySelector('input#name_class_ua[name="name_class_ua"]').value.length !== 0){
                data.nameClassUA = document.querySelector('input#name_class_ua[name="name_class_ua"]').value.toString();
            }
        }
        // fetch response
        const response = await fetch('/adminpanel/saveClass', {
            method: 'POST', // или 'PUT'
            body: JSON.stringify(data), // данные могут быть 'строкой' или {объектом}!
            headers: {
                'Content-Type': 'application/json'
            }
        });
        document.querySelector('input#name_class').value = '';
        document.querySelector('input#name_class_ua').value = '';
        //if response ok
        let json = await response.json();
        if(response.ok && response.status === 200){
            let htmlClass = `<li class="waves-effect"><a href="" data-id="${json._id}">${json.NumberClass}</a></li>`;
            document.querySelector('ul#p_allLi').insertAdjacentHTML('beforeend', htmlClass);
        }else if (response.ok && response.status === 202){
            if (document.querySelector('li.active.teal') && document.querySelector('li.active.teal').firstElementChild.dataset.id === json[0]._id){
                    let htmlClass = `<div class="chip ">${json[0].NameClass[json[0].NameClass.length-1].text}<i data-idx="${json[0].NameClass.length-1}" class="close material-icons d_name_cl">close</i></div>`;
                   if(json[0].NameClass[json[0].NameClass.length-1].language === 'ru'){
                       document.querySelector('div#class_name_text').insertAdjacentHTML('beforeend', htmlClass);
                   }else if(json[0].NameClass[json[0].NameClass.length-1].language === 'ua') {
                       document.querySelector('div#class_name_text_ua').insertAdjacentHTML('beforeend', htmlClass);

                   }
            }
        }else if (response.ok && response.status === 203){
            if (document.querySelector('li.active.teal') && document.querySelector('li.active.teal').firstElementChild.dataset.id === json[0]._id){

                console.log(json);
                let htmlClass = `<div class="chip ">${json[0].NameClass[json[0].NameClass.length-2].text}<i data-idx="${json[0].NameClass.length-2}" class="close material-icons d_name_cl">close</i></div>`;
                document.querySelector('div#class_name_text').insertAdjacentHTML('beforeend', htmlClass);
                let htmlClassUA = `<div class="chip ">${json[0].NameClass[json[0].NameClass.length-1].text}<i data-idx="${json[0].NameClass.length-1}" class="close material-icons d_name_cl">close</i></div>`;
                document.querySelector('div#class_name_text_ua').insertAdjacentHTML('beforeend', htmlClassUA);

            }
            }


    }catch (e) {
        console.log(e);
    }
})
// end click save button

// select class
document.querySelector('ul#p_allLi').addEventListener('click', async (e)=>{
    try{
        e.preventDefault();
        if(e.target.closest('li.waves-effect')){
            document.getElementById('class_name_text_parent').classList.remove('d_n_class');


            let data = {};
            if (document.querySelector('li.active.teal')){
                document.querySelector('li.active.teal').classList.add('waves-effect');
                data.id=document.querySelector('li.active.teal').firstElementChild.dataset.id
                document.querySelector('li.active.teal').classList.remove('active', 'teal');


             }
            e.target.closest('li.waves-effect').classList.add('active', 'teal');
            data.id=document.querySelector('li.active.teal').firstElementChild.dataset.id
            e.target.closest('li.waves-effect').classList.remove('waves-effect');
            document.querySelector('p.select_paggination_num').textContent = e.target.closest('li').textContent;
            document.querySelector('div#class_name_text').textContent = '';
            document.querySelector('div#class_name_text_ua').textContent = '';
            const response =  await fetch('/adminpanel/get', {
                method: 'POST',
                body: JSON.stringify(data), // данные могут быть 'строкой' или {объектом}!
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            let json = await response.json();

            if (response.ok){
                json.NameClass.forEach((classN, index )=>{
                    let htmlClass = `<div class="chip ">${classN.text}<i data-idx="${index}" class="close material-icons d_name_cl">close</i></div>`;
                    if(classN.language === 'ru'){
                        document.querySelector('div#class_name_text').insertAdjacentHTML('beforeend', htmlClass);
                    }else if (classN.language === 'ua'){
                        document.querySelector('div#class_name_text_ua').insertAdjacentHTML('beforeend', htmlClass);
                    }


                })
            }
        }

    }catch (e) {
        console.log(e)
    }

})
//

//pagination
document.querySelector('div#class_name_text_parent').addEventListener('click', async e=>{
    try{
        if(e.target.closest('i.d_name_cl')){
            let data = {}
            data.id = document.querySelector('li.active.teal').firstElementChild.dataset.id;
            data.idx = e.target.closest('i.d_name_cl').dataset.idx;
            data.text = e.target.closest('i.d_name_cl').parentElement.firstChild.textContent;

            const response = await fetch('/adminpanel/dClassName', {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            //let json = await response.json();
            if (response.ok){

            }
        }
    }catch (e) {
        console.log(e)
    }
})
//

// update header
document.querySelector('div#change__header').addEventListener('change', async ()=>{
    try{
        let data = {};
       document.getElementById('change__header').querySelectorAll('input.validate').forEach(element =>{
           data[element.getAttribute('name')] = element.value;
       });
        // fetch response
        const response = await fetch('/adminpanel/updateEdges', {
            method: 'POST', // или 'PUT'
            body: JSON.stringify(data), // данные могут быть 'строкой' или {объектом}!
            headers: {
                'Content-Type': 'application/json'
            }
        });
        //if response ok
        if(response.ok){

        }
    }catch (e) {
        console.log(e)
    }
})
//

// update footer
document.querySelector('div#change__footer').addEventListener('change', async ()=>{
    try{
        let data = {};
        document.getElementById('change__footer').querySelectorAll('input.validate').forEach(element =>{
            data[element.getAttribute('name')] = element.value;
        });
        // fetch response
        const response = await fetch('/adminpanel/updateEdges', {
            method: 'POST', // или 'PUT'
            body: JSON.stringify(data), // данные могут быть 'строкой' или {объектом}!
            headers: {
                'Content-Type': 'application/json'
            }
        });
        //if response ok
        if(response.ok){

        }
    }catch (e) {
        console.log(e)
    }
});
//

//find Question
document.querySelector('div#ch__footer').addEventListener('click', async e=>{
    e.preventDefault();
    try {
    if(e.target.closest('button#save__question.save-cla')){
        //new Question
        if(document.querySelector('input#quest-title.validate[name="Question"]').value === ''){
         alert('Введите вопрос');
        }else if(document.querySelector('textarea#textarea1[name="Answer"]').value === ''){
            alert('Введите ответ');
        }else{
            let data = {};
            data.language = document.getElementById('ch__footer').querySelector('.active_lang').dataset.lang
            data[document.querySelector('input#quest-title.validate[name="Question"]').getAttribute('name')] =  document.querySelector('input#quest-title.validate[name="Question"]').value;
            data[document.querySelector('textarea#textarea1[name="Answer"]').getAttribute('name')] =  document.querySelector('textarea#textarea1[name="Answer"]').value;
            data.id = document.querySelector('input#id_edit').value;
            // fetch response
            const response = await fetch('/adminpanel/newQ', {
                method: 'POST', // или 'PUT'
                body: JSON.stringify(data), // данные могут быть 'строкой' или {объектом}!
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            document.querySelector('input#id_edit').value = '';
            let json = await response.json();
            //if response ok
            if(response.ok && response.status === 200){
                let htmlQ = `<li class="collection-item quest__li "><div>${json.Question}<a href="" data-language="${json.language}" data-answer="${json.Answer}" data-id="${json._id}" class="secondary-content id__e_element"><i class="material-icons editEl">create</i>&nbsp;<i class="material-icons editDel">clear</i></a></div></li>`;
                document.querySelector('ul#list__question').insertAdjacentHTML('beforeend', htmlQ);
                document.querySelector('input#quest-title').value = '';
                document.querySelector('textarea#textarea1').value = '';
            }else if (response.ok && response.status === 202){
                document.querySelector('li.check_edit_li_q').firstElementChild.firstChild.textContent = json.Question;
                document.querySelector('li.check_edit_li_q').firstElementChild.firstElementChild.dataset.id = json._id;
                document.querySelector('li.check_edit_li_q').firstElementChild.firstElementChild.dataset.answer = json.Answer;
                document.querySelector('li.check_edit_li_q').firstElementChild.firstElementChild.dataset.language = json.language;
                document.querySelector('li.check_edit_li_q').classList.remove('check_edit_li_q');
                document.querySelector('input#quest-title').value = '';
                document.querySelector('textarea#textarea1').value = '';
            }
        }
    }

    //click Edit question
    if (e.target.closest('i.editEl')){
        //question
        document.querySelector('input#quest-title.validate[name="Question"]').value = e.target.closest('li.collection-item').firstElementChild.firstChild.textContent;
        document.querySelector('input#quest-title.validate[name="Question"]').nextElementSibling.classList.add('active');
        //answer
        document.querySelector('textarea#textarea1[name="Answer"]').value = e.target.parentElement.dataset.answer;
        document.querySelector('textarea#textarea1[name="Answer"]').nextElementSibling.classList.add('active');
        //id
        document.querySelector('input#id_edit[name="id"]').value = e.target.closest('a.id__e_element').dataset.id;
        e.target.closest('li.collection-item').classList.add('check_edit_li_q')

    }

    //click delete question
    if (e.target.closest('i.editDel')){
        let data = {id: e.target.closest('a.id__e_element').dataset.id};
        // fetch response
        const response = await fetch('/adminpanel/d', {
            method: 'POST', // или 'PUT'
            body: JSON.stringify(data), // данные могут быть 'строкой' или {объектом}!
            headers: {
                'Content-Type': 'application/json'
            }
        });
        //if response ok

        if(response.ok){
            //const json = await response.json();
            e.target.closest('li.quest__li').remove();
        }
    }

    }catch (e) {
        console.log(e)
    }
})
//

//Search 
document.querySelector('div#ch__search').addEventListener('click', async e=>{
    try {
    e.preventDefault();
    //add new Search text
    if (e.target.closest('button#save__search')) {
        if(document.querySelector('input#quest-s').value === ''){
        }else{
            let data = {
                SearchText: document.querySelector('input#quest-s').value
            }
            data.language = document.getElementById('ch__search').querySelector('.active_lang').dataset.lang
            data.id = document.querySelector('input#id_editSearch').value;
            // fetch response
            const response = await fetch('/adminpanel/addSearch', {
                method: 'POST',
                body: JSON.stringify(data), // данные могут быть 'строкой' или {объектом}!
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            document.querySelector('input#id_editSearch').value = '';
        //if response ok
            let json = await response.json();
        if(response.ok && response.status === 200){
            let html = `<li class="collection-item text_item_search"><div>${json.SearchText}<a href="#" data-language="${json.language}" data-id="${json._id}" class="secondary-content search__id"><i class="material-icons search_edit">create</i>&nbsp;<i class="material-icons search_delete">clear</i></a></div></li>`;
            document.querySelector('ul.text__search_ul').insertAdjacentHTML('beforeend', html);
            document.querySelector('input#quest-s').value = '';
        }else if (response.ok && response.status === 202){
            document.querySelector('li.check_edit_li_a').firstElementChild.firstChild.textContent = json.SearchText;
            document.querySelector('li.check_edit_li_a').firstElementChild.firstChild.dataset.language = json.language;
            document.querySelector('input#quest-s').value = '';
            document.querySelector('li.check_edit_li_a').classList.remove('check_edit_li_a');

        }
        }
    }
    // Edit Search
    if (e.target.closest('i.search_edit')) {
        //text
        e.target.closest('li.collection-item').classList.add('check_edit_li_a');
        document.querySelector('input#quest-s').value = e.target.closest('li.collection-item').firstElementChild.firstChild.textContent;
        document.querySelector('input#quest-s').nextElementSibling.classList.add('active');
        //id
         document.querySelector('input#id_editSearch').value = e.target.closest('a.search__id').dataset.id;
    }
    //Delete Search
    if (e.target.closest('i.search_delete')) {
        let data = {id: e.target.closest('a.search__id').dataset.id};
        // fetch response
        const response = await fetch('/adminpanel/ds', {
            method: 'POST', // или 'PUT'
            body: JSON.stringify(data), // данные могут быть 'строкой' или {объектом}!
            headers: {
                'Content-Type': 'application/json'
            }
        });
       

        //if response ok
        if(response.ok){
            //const json = await response.json();
            e.target.closest('li.collection-item').remove();
        }
    }
} catch (error) {
        
}
})
//

//Update About
document.querySelector('div#block__about_me').addEventListener('change', async ()=>{
    try{
        let data = {};
        data.language = document.getElementById('block__about_me').querySelector('.active_lang').dataset.lang
        document.getElementById('block__about_me').querySelectorAll('input.validate').forEach(element =>{
            data[element.getAttribute('name')] = element.value;
        });
        data.Text = document.querySelector('textarea#textarea2About').value;
        // fetch response
        const response = await fetch('/adminpanel/upMe', {
            method: 'POST', // или 'PUT'
            body: JSON.stringify(data), // данные могут быть 'строкой' или {объектом}!
            headers: {
                'Content-Type': 'application/json'
            }
        });
        //if response ok
        if(response.ok){

        }
    }catch (e) {
        console.log(e)
    }
})
//

//input file
document.querySelector('input#our-img[type="file"]').addEventListener('change', async ()=>{
        let curFiles = document.querySelector('input#our-img[type="file"]').files;
        let image = document.createElement('img');
        image.classList.add('show');
        // document.querySelector('img#imgBrand');
        image.src = window.URL.createObjectURL(curFiles[0]);
        document.querySelector('label#label_img').innerHTML = '';
        document.querySelector('label#label_img').classList.remove('choose_img');
        document.querySelector('label#label_img').appendChild(image);
})
//

//Our brand
document.querySelector('div#block__our_brand').addEventListener('click', async (e)=>{
    try {
        //new brand or update
        if (e.target.closest('button#save__ourbrand')){
            e.preventDefault();
            let data = new FormData();
            let img = document.querySelector('input#our-img').files;
            data.append(document.querySelector('input#id_editOurBrand[name="id"]').getAttribute('id'), document.querySelector('input#id_editOurBrand[name="id"]').value);
            data.append(document.querySelector('input#f-urlBrand[name="urlBrand"]').getAttribute('name'), document.querySelector('input#f-urlBrand[name="urlBrand"]').value);
            data.append(document.querySelector('input#our-img[name="img"]').getAttribute('name'), img[0]);
            data.append(document.querySelector('input#f-nameBrand[name="NameBrand"]').getAttribute('name'), document.querySelector('input#f-nameBrand[name="NameBrand"]').value);
            const response = await fetch('/adminpanel/sBrand', {
                method: 'POST', // или 'PUT'
                body: data, // данные могут быть 'строкой' или {объектом}!
            });
            //if response ok
            document.querySelector('input#f-urlBrand[name="urlBrand"]').value = '';
            document.querySelector('input#f-nameBrand[name="NameBrand"]').value = '';
            document.querySelector('input#id_editOurBrand').value = '';
            document.querySelector('label#label_img').innerHTML = 'Выбирите изображение';
            document.querySelector('label#label_img').classList.add('choose_img');
            let json = await response.json();
            if(response.ok && response.status === 200){
                let htmlB = `<li class="collection-item"><div>${json.NameBrand}<a href="#" data-id="${json._id}" data-img="${json.img}" data-url="${json.urlBrand}" class="secondary-content brand_link"><i class="material-icons brand_edit">create</i>&nbsp;<i class="material-icons brand_delete">clear</i></a></div></li>`;
                document.querySelector('ul#brand__list').insertAdjacentHTML('beforeend', htmlB);

            }else if(response.ok && response.status === 202){
                document.querySelector('li.li_check_brand').firstElementChild.firstChild.textContent = json.NameBrand;
                document.querySelector('li.li_check_brand').firstElementChild.firstElementChild.dataset.id = json._id;
                document.querySelector('li.li_check_brand').firstElementChild.firstElementChild.dataset.url = json.urlBrand;
                document.querySelector('li.li_check_brand').firstElementChild.firstElementChild.dataset.img = json.img;
                document.querySelector('li.li_check_brand').classList.remove('li_check_brand');
            }
        }
        //edit brand
        if (e.target.closest('i.brand_edit')){
            e.preventDefault();
            e.target.closest('li.collection-item').classList.add('li_check_brand');
            document.querySelector('input#f-urlBrand[name="urlBrand"]').value = e.target.closest('a.brand_link').dataset.url;
            document.querySelector('input#f-nameBrand[name="NameBrand"]').value = e.target.closest('li.collection-item').firstElementChild.firstChild.textContent;
            document.querySelector('input#id_editOurBrand').value = e.target.closest('a.brand_link').dataset.id;
            let image = document.createElement('img');
            image.classList.add('show');
            // document.querySelector('img#imgBrand');
            image.src = e.target.closest('a.brand_link').dataset.img;
            document.querySelector('label#label_img').innerHTML = '';
            document.querySelector('label#label_img').classList.remove('choose_img');
            document.querySelector('label#label_img').appendChild(image);
        }

        //delete brand
        if(e.target.closest('i.brand_delete')){
            e.preventDefault();
            let data = {
                id: e.target.closest('a.brand_link').dataset.id
            }

            const response = await fetch('/adminpanel/dBrand', {
                method: 'POST', // или 'PUT'
                body: JSON.stringify(data), // данные могут быть 'строкой' или {объектом}!
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            if(response.ok){
                e.target.closest('li.collection-item').remove();
            }
        }
    }catch (e) {
        console.log(e)
    }
})
//

//news
document.querySelector('div#block_news').addEventListener('click', async (e)=>{
    try{

        if(e.target.closest('button#save__news')){
            e.preventDefault();
            let data = {};
            document.getElementById('block_news').querySelectorAll('input.validate').forEach(element =>{
                data[element.getAttribute('name')] = element.value;
            });
            data[document.querySelector('textarea#textarea3News').getAttribute('name')] = document.querySelector('textarea#textarea3News').value;
            const response = await fetch('/adminpanel/news', {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            document.querySelector('input#f-yearNews').value = '';
            document.querySelector('input#f-titleNews').value = '';
            document.querySelector('textarea#textarea3News').value = '';
            document.querySelector('textarea#textarea3News').nextElementSibling.classList.remove('active');
            document.querySelector('input#id_editnews').value = '';
            let json = await response.json();
            if(response.ok && response.status === 200){
                let htmlN = `<li class="collection-item"><div>${json.newsTitle}<a href="#" data-id="${json._id}" data-year="${json.newsDate}" data-text="${json.newsText}" class="secondary-content news_link"><i class="material-icons news_edit">create</i>&nbsp;<i class="material-icons news_delete">clear</i></a></div></li>`;
                document.querySelector('ul#news__list').insertAdjacentHTML('beforeend', htmlN);

            }else if(response.ok && response.status === 202){
                document.querySelector('li.check_li_news').firstElementChild.firstChild.textContent = json.newsTitle;
                document.querySelector('li.check_li_news').firstElementChild.firstElementChild.dataset.id = json._id;
                document.querySelector('li.check_li_news').firstElementChild.firstElementChild.dataset.year = json.newsDate;
                document.querySelector('li.check_li_news').firstElementChild.firstElementChild.dataset.text = json.newsText;
               document.querySelector('li.check_li_news').classList.remove('check_li_news');
            }
        }
        if(e.target.closest('i.news_edit')){
            e.preventDefault();
            e.target.closest('li.collection-item').classList.add('check_li_news');
            document.querySelector('input#f-yearNews').value =  e.target.closest('a.news_link').dataset.year;
            document.querySelector('input#f-titleNews').value = e.target.closest('li.collection-item').firstElementChild.firstChild.textContent;
            document.querySelector('textarea#textarea3News').value = e.target.closest('a.news_link').dataset.text;
            document.querySelector('textarea#textarea3News').nextElementSibling.classList.add('active');
            document.querySelector('input#id_editnews').value = e.target.closest('a.news_link').dataset.id;
        }
        if(e.target.closest('i.news_delete')){
            e.preventDefault();
            let data = {id: e.target.closest('a.news_link').dataset.id};
            const response = await fetch('/adminpanel/dNews', {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            if (response.ok){
                e.target.closest('li').remove();
            }
        }
    }catch (e) {
        console.log(e)
    }
})
//

//log out

document.querySelector('.logout').addEventListener('click', async (e)=>{
    e.preventDefault();
    try {
        const response = await fetch('/adminpanel/logout', {
            method: 'POST',
        })
        if (response.ok){
            document.location.href = response.url;
        }
    }catch (e) {
        console.log(e)
    }
})

