var validate = false;
var forMail = false;
document.querySelectorAll('.tab_triger__item').forEach(item=>{
    item.addEventListener('click', e=>{
        e.preventDefault();
        const id = e.target.getAttribute('href').replace('#', '');

        document.querySelectorAll('.tab_triger__item').forEach(child=>{
            child.classList.remove('tab_triger__item-active')
        });
        document.querySelectorAll('.tab_content').forEach(child=>{
            child.classList.remove('tab_content-active')
        });

        item.classList.add('tab_triger__item-active');
        document.getElementById(id).classList.add('tab_content-active');

    })
})
function back(c) {
    document.querySelector(`.${c}`).classList.remove('active');
    document.querySelector(`.${c}`).previousElementSibling.classList.add('active');
}
function tab1(c, f) {
    document.getElementById('tabs_tr1').click();
    document.querySelector(`.${c}`).classList.remove('active');
    document.querySelector(`.${f}`).classList.add('active');
}
function tab3(c, f) {
    document.getElementById('tabs_tr3').click();
    document.querySelector(`.${c}`).classList.remove('active');
    document.querySelector(`.${f}`).classList.add('active');
}
function stepBack(){
    const registration__card  = document.querySelector('.registration__card.active');
    const step1  = document.querySelector('.registration__card.s1');
    registration__card.classList.remove('active');
    step1.classList.add('active');
}
function popapSubmit(par, obj) {
    //validateForm(par);
    function dotsAnimation(){
        let dots = $('.loading-dots span');
        let a = 0;
        if ($('.loading-dots span.active').index() === dots.length - 1 || a ===0 ){
            $('.loading-dots span').eq(0).addClass('active').siblings().removeClass('active');
            a++;
        }
        else {
            $('.loading-dots span.active').next().toggleClass('active').siblings().removeClass('active');
        }
    }
    function sendEmail(par) {
        validate = false;
        var data = '';
        var request = new XMLHttpRequest();
        var url = "/users";

        request.open("POST", url, true);
        request.setRequestHeader("Content-Type", "application/json");
        request.upload.onprogress = function (event) {
            $('.registration__card').removeClass('active').fadeOut('slow');
            $('.registration__card_type_loading').addClass('active').fadeIn('slow');
            dotsAnimation();
            //console.log(event);
        };
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                var jsonData = JSON.parse(request.response);
                /*obj.form.reset();*/
                //console.log(jsonData);
                $('#registration__card').fadeOut('slow').removeClass('active');
                $('.registration__card.thanks').fadeIn('slow').addClass('active');
                // if (par == 'form') {
                //     document.getElementById('one-el').style.display = 'none';
                //     document.getElementById('two-el').style.display = 'flex';
                //     document.getElementById('send_form_1').style.display = 'none';
                //     document.getElementById('send_form_2').style.display = 'flex';
                //     document.getElementById('ins-two-t').innerHTML = jsonData;
                // } else {
                //
                // }
                /* setTimeout(() => document.getElementById('verify-form').style.display = 'none', 4000);*/
            }
        };
        data = '';
        var name;
        var tel;
        if (par == "true") {
          name =  document.getElementById("s_name").value;
          tel = document.getElementById("tel_s").value;
        } else {
          name =  document.getElementById("s_name_false").value;
          tel = document.getElementById("s_tel_true").value;
        }

        var k =  document.getElementById("kBr").value;
        var nB = document.getElementById("nameBr").value;
        var nameC = document.getElementById("nameC").value;

        data = JSON.stringify({"successfully_name": name, "successfully_tel": tel, "key_br": k, "name_br": nB, "name_k": nameC});
        request.send(data);
    }
    sendEmail(par);
    // (validate) ? sendEmail(par) : console.log('not validate form');
}

function printError(elemId, hintMsg) {
    document.getElementById(elemId).innerHTML = hintMsg;
    console.log(elemId);
}
// Defining a function to validate form

function validateForm(par) {
    // Defining a function to display error message
    (par == 'form') ? contactForm() : contactForm2();

    function contactForm() {
        var name    = document.form.name.value;
        var name2    = document.form.names.value;

        // Defining error variables with a default value
        var nameErr = nameErr2 = true;

        // Validate name
        if(name == "") {
            printError("nameErr", "Please enter your name");
        } else {
          /*  var regex = /^[a-zA-Z\s]+$/;
            if(regex.test(name) === false) {
                printError("nameErr", "Please enter a valid name");
            } else {
                printError("nameErr", "");
                nameErr = false;
            }*/
            nameErr = false;
        }
        if(name2 == "") {
            printError("nameErr2", "Please enter your name");
        } else {
         /*   var regex = /^[a-zA-Z\s]+$/;
            if(regex.test(name2) === false) {
                printError("nameErr2", "Please enter a valid name");
            } else {
                printError("nameErr2", "");
                nameErr2 = false;
            }*/
            nameErr2 = false;
        }



        // Prevent the form from being submitted if there are any errors
        if((nameErr || nameErr2 ) == true) {
            return false;
        } else {
            validate = true;
        }
    }
    function contactForm2() {
        var name    = document.form.name22.value;
        var name2    = document.form.names33.value;

        // Defining error variables with a default value
        var nameErr = nameErr2 = true;

        // Validate name
        if(name == "") {
            printError("nameErr22", "Please enter your name");
        } else {
          /*  var regex = /^[a-zA-Z\s]+$/;
            if(regex.test(name) === false) {
                printError("nameErr", "Please enter a valid name");
            } else {
                printError("nameErr", "");
                nameErr = false;
            }*/
            nameErr = false;
        }
        if(name2 == "") {
            printError("nameErr33", "Please enter your text");
        } else {
         /*   var regex = /^[a-zA-Z\s]+$/;
            if(regex.test(name2) === false) {
                printError("nameErr2", "Please enter a valid name");
            } else {
                printError("nameErr2", "");
                nameErr2 = false;
            }*/
            nameErr2 = false;
        }



        // Prevent the form from being submitted if there are any errors
        if((nameErr || nameErr2 ) == true) {
            return false;
        } else {
            validate = true;
        }
    }
};





