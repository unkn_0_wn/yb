(function (){
    const divSelect = document.querySelector('div.input_select_list2');
    const divP =  document.getElementById('parent_select_box2');
    const input = document.querySelector('input[type=text].filter_select2');
    const containerList = document.querySelector('div.item_list2');
    const ul = document.getElementById('ul_select_list2');
    const select = document.getElementById('tokenfield2');
    const count_selectItem = 1;

// divSelect.addEventListener('click', e=>{
//       input.focus();
//       containerList.classList.add('active');
// })
    function chekCountElement (t, e){

        if(select.selectedOptions.length == count_selectItem){
            let liResult = ul.querySelectorAll('li.list_item_li2');
            liResult.forEach(item =>{
                item.classList.add('display');

            })
            let liAppend = document.createElement('li');
            liAppend.classList.add('n_result2');
            liAppend.innerHTML = `Можно добавить только ${count_selectItem} значение`;
            ul.append(liAppend);
        }
        if (select.selectedOptions.length == 0){
            let liitem= ul.querySelectorAll('li.list_item_li2');

            liitem.forEach(item =>{
                item.classList.remove('display');

            })
            let liResult = ul.querySelectorAll('li.n_result2');
            liResult.forEach(i =>{
                i.remove()
            })

        }
//      if (select.selectedOptions.length > 0 && select.selectedOptions.length < count_selectItem){
//           let liitem= ul.querySelectorAll('li.list_item_li');
//           let Arropt = [];
//             for (let i = 0; i < select.selectedOptions.length; i++){
//                  Arropt.push(select.selectedOptions[i].innerHTML.toString());
//             }


//            liitem.forEach(item =>{


//                 if (item.innerHTML.toString() === t  || !~Arropt.indexOf(item.innerHTML.toString())){
//                      item.classList.remove('display');
//            }


//        })

//           }
    }
    function renderCheckItem (e){
        if(select.selectedOptions.length != 0 ){
            let spanCreate = document.createElement('span');
            let buttonCreate = document.createElement('button');
            spanCreate.classList.add('selectitem2');
            for (let i = 0; i < select.selectedOptions.length; i++){

                spanCreate.innerHTML=select.selectedOptions[i].innerHTML;
            }
            buttonCreate.classList.add('d_item_list');
            buttonCreate.type = 'text';
            buttonCreate.innerHTML= '&#10006;';
            spanCreate.append(buttonCreate);
            input.before(spanCreate);
            let s =  document.querySelector('span.selectitem2');
            input.style.paddingLeft = `${s.offsetWidth + 20}px `;
            input.value = '' ;
            chekCountElement(e);
        }

    }
    function deleteCheckItem(e){
        let dItem = e.closest('.selectitem2').innerText;
        dItem = dItem.substring(0, dItem.length - 1);
        let liitem= ul.querySelectorAll('li.list_item_li2');
        for (let i = 0; i < select.selectedOptions.length; i++){
            let hhh = select.selectedOptions[i].innerHTML.toString();

            if(dItem.toString() === select.selectedOptions[i].innerHTML.toString()){
                select.selectedOptions[i].remove();
            }

        }
        let liResult = ul.querySelectorAll('li.n_result2');
        liResult.forEach(i =>{
            i.remove()
        })
        let idx = select.selectedOptions[0];
        e.closest('.selectitem2').remove();
        chekCountElement(dItem, e);

    }

    input.addEventListener('focus', e=>{
        containerList.classList.add('active');
    })


    input.addEventListener('input', e=>{
        let li = ul.querySelectorAll('li');
        let check = false;
        li.forEach(text => {
            if (select.selectedOptions.length == count_selectItem) {
                input.value = "";
            } else {

                if (text.innerHTML.toLowerCase().includes(e.target.value.toLowerCase())) {
                    text.classList.remove('display')
                } else {
                    text.classList.add('display')
                }
                if (!text.classList.contains('display')) {
                    check = true
                }
            }

        })
        if (select.selectedOptions.length != count_selectItem) {
            if (!check) {
                let liAppend = document.createElement('li');
                liAppend.classList.add('n_result2');
                liAppend.innerHTML = 'Не найдено результатов поиска';
                ul.append(liAppend);
            } else {

                let liResult = ul.querySelectorAll('li.n_result2');
                liResult.forEach(item => {
                    item.remove()
                })
            }
        }
    })

    divSelect.addEventListener('click', e=>{
        if(e.target.closest('.d_item_list')){
            deleteCheckItem(e.target);
            document.querySelector('input#kBr').value= '';
            document.querySelector('input#nameBr').value = '';
            input.style.paddingLeft = '30px';
            ul.parentElement.classList.remove('mobile_select_inp');
            document.querySelector('input#mobile_brandd2.filter_select2').classList.remove('mobile_select_inp2');
            document.querySelector('input#id_brand2[name=name_brand2]').classList.remove('mobile_select_inp2');

        }
    })

    ul.addEventListener('click', e=>{
        if(e.target.closest('.list_item_li2')){
            input.focus();
            ul.parentElement.classList.add('mobile_select_inp');
            document.querySelector('input#mobile_brandd2.filter_select2').classList.add('mobile_select_inp2');
            document.querySelector('input#id_brand2[name=name_brand2]').classList.add('mobile_select_inp2');
            let opt = document.createElement("option");
            opt.value = e.target.innerHTML.toString();
            opt.text = e.target.innerHTML.toString();
            document.querySelector('input#kBr').value = e.target.dataset.cod;
            document.querySelector('input#nameBr').value = e.target.innerHTML.toString();
            opt.value = e.target.dataset.cod;
            opt.setAttribute('selected', '')
            select.add(opt);
            e.target.classList.add('display');
            renderCheckItem(e);
        }
    })
    document.addEventListener('click', e=>{
        if(!e.target.closest('#parent_select_box2')){
            containerList.classList.remove('active');
            input.blur();
        }
    })














}())